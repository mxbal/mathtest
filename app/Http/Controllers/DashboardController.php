<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $questions = Question::get();

        return view('dashboard.index', compact('questions'));
    }

    public function profile()
    {
        $user = User::find(auth()->user()->id);

        return view('dashboard.profile', compact('user'));
    }

    public function update(User $user, Request $request)
    {
        $attr = $request->validate([
            'username' => 'required|unique:users,username,' . $user->id,
            'name' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $attr['password'] = $request->password != NULL ? bcrypt($request->password) : $user->password;

            if ($user->level == 'Admin') {
                $user->update($attr);
            } else {
                $user->update($attr);

                $user->siswa->update([
                    'nis' => $request->username,
                    'nama' => $request->name,
                    'kelas' => $request->kelas,
                ]);
            }

            DB::commit();

            return back()->with('success', 'Profile berhasil diupdate');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }
}
