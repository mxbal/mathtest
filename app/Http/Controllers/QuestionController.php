<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{

    public function __construct()
    {
        $this->middleware('isadmin')->except('show', 'find', 'answer');
    }

    public function index()
    {
        $questions = Question::get();

        return view('question.index', compact('questions'));
    }

    public function create()
    {
        $question = new Question();

        return view('question.create', compact('question'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'nama' => 'required',
            'soal' => 'required',
            'jawaban' => 'required',
            'batas_atas' => 'required',
            'batas_bawah' => 'required',
            'bobot_nilai' => 'required',
        ]);

        try {
            DB::beginTransaction();

            Question::create($attr);

            DB::commit();

            return redirect()->route('question.index')->with('success', 'Soal berhasil ditambahkan');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(Question $question)
    {
        return view('question.show', compact('question'));
    }

    public function edit(Question $question)
    {
        return view('question.edit', compact('question'));
    }

    public function update(Request $request, Question $question)
    {
        $attr = $request->validate([
            'nama' => 'required',
            'soal' => 'required',
            'jawaban' => 'required',
            'batas_atas' => 'required',
            'batas_bawah' => 'required',
            'bobot_nilai' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $question->update($attr);

            DB::commit();

            return redirect()->route('question.index')->with('success', 'Soal berhasil diupdate');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Question $question)
    {
        try {
            DB::beginTransaction();

            $question->siswa()->detach();
            $question->delete();

            DB::commit();

            return redirect()->route('question.index')->with('success', 'Soal berhasil didelete');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function find(Question $question)
    {
        return response()->json([
            'question' => $question
        ]);
    }

    public function answer(Request $request)
    {
        $request->validate(
            [
                'deskripsi' => 'required',
                'jawaban' => 'required',
            ],
            [
                'deskripsi.required' => 'Deskripsi harus di isi',
                'jawaban.required' => 'Jawaban harus di isi',
            ]
        );
        try {
            $question = Question::find($request->question_id);

            $data = ['deskripsi' => $request->deskripsi, 'jawaban' => $request->jawaban, 'nilai' => $request->nilai];

            $question->siswa()->attach($request->siswa_id, $data);

            return redirect()->route('dashboard')->with('success', 'Soal telah dijawab');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
