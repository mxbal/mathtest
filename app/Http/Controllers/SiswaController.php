<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiswaController extends Controller
{
    public function index()
    {
        $siswa = Siswa::get();

        return view('siswa.index', compact('siswa'));
    }

    public function create()
    {
        $siswa = new Siswa();

        return view('siswa.create', compact('siswa'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'nis' => 'required|unique:siswas',
            'nama' => 'required',
            'kelas' => 'required'
        ]);

        try {
            DB::beginTransaction();

            $user = User::create([
                'username' => $request->nis,
                'name' => $request->nama,
                'password' => bcrypt($request->nis),
                'level' => 'Siswa'
            ]);

            $user->siswa()->create($attr);

            DB::commit();

            return redirect()->route('siswa.index')->with('success', 'Siswa berhasil ditambahkan');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(Siswa $siswa)
    {
        return view('siswa.show', compact('siswa'));
    }

    public function edit(Siswa $siswa)
    {
        return view('siswa.edit', compact('siswa'));
    }

    public function update(Request $request, Siswa $siswa)
    {
        $attr = $request->validate([
            'nis' => 'required|unique:siswas,nis,' . $siswa->id,
            'nama' => 'required',
            'kelas' => 'required'
        ]);

        try {
            DB::beginTransaction();

            $user = User::find($siswa->user_id);

            $user->update([
                'username' => $request->nis,
                'name' => $request->nama,
                'password' => bcrypt($request->nis),
                'level' => 'Siswa'
            ]);

            $user->siswa()->update($attr);

            DB::commit();

            return redirect()->route('siswa.index')->with('success', 'Siswa berhasil diupdate');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Siswa $siswa)
    {
        try {
            DB::beginTransaction();

            $siswa->user()->delete();
            $siswa->delete();

            DB::commit();

            return redirect()->route('siswa.index')->with('success', 'Siswa berhasil didelete');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function nilai()
    {
        $siswa = Siswa::get();

        return view('siswa.nilai', compact('siswa'));
    }

    public function reset(Siswa $siswa)
    {
        $siswa->questions()->detach();

        return back()->with('success', 'Reset nilai siswa berhasil');
    }
}
