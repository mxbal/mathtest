<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('level', 'Admin')->get();

        return view('users.index', compact('users'));
    }

    public function create()
    {
        $user = new User();
        return view('users.create', compact('user'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'username' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $attr['password'] = bcrypt($request->password);
            $attr['level'] = 'Admin';

            User::create($attr);

            DB::commit();

            return redirect()->route('users.index')->with('success', 'Admin berhasil ditambahkan');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $attr = $request->validate([
            'username' => 'required|unique:users,username,' . $user->id,
            'name' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $attr['password'] = $request->password != NULL ? bcrypt($request->password) : $user->password;
            $attr['level'] = 'Admin';

            $user->update($attr);

            DB::commit();

            return redirect()->route('users.index')->with('success', 'Admin berhasil diupdate');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(User $user)
    {
        try {
            DB::beginTransaction();

            $user->delete();

            DB::commit();

            return redirect()->route('users.index')->with('success', 'Admin berhasil didelete');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }
}
