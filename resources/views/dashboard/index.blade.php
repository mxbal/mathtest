@extends('layouts.master', ['title' => 'Dashboard'])

@section('content')
@if(auth()->user()->level == 'Admin')
<div class="row">
    <div class="col-lg-4 col-6">
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>{{ App\Models\User::where('level', 'Admin')->count() }}</h3>

                <p>Jumlah Admin</p>
            </div>
            <div class="icon">
                <i class="fas fa-user-shield"></i>
            </div>
            <a href="{{ route('users.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-4 col-6">
        <div class="small-box bg-info">
            <div class="inner">
                <h3>{{ App\Models\Siswa::count() }}</h3>

                <p>Jumlah Siswa</p>
            </div>
            <div class="icon">
                <i class="fas fa-users"></i>
            </div>
            <a href="{{ route('siswa.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-4 col-6">
        <div class="small-box bg-success">
            <div class="inner">
                <h3>{{ App\Models\Question::count() }}</h3>

                <p>Jumlah Soal</p>
            </div>
            <div class="icon">
                <i class="fas fa-question-circle"></i>
            </div>
            <a href="{{ route('question.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
@else
<p>
<h5>Aturan Soal</h5>
1. Selesaikan setiap soal untuk membuka soal berikutnya. <br>
2. Anda hanya diperbolehkan menjawab satu kali. <br>
3. Jika terjadi kesalahan, mintalah admin/guru untuk mereset soal.

</p>
<div class="row">
    @foreach($questions as $question)
    <div class="col-md-4">
        <div class="card card-primary card-outline">
            <div class="card-body">
                <h5 class="mb-3">{{ $question->nama }}</h5>

                @if(in_array(auth()->user()->siswa->id, $question->siswa()->pluck('siswa_id')->toArray()))
                <p>
                    <b>Jawaban : </b> {{ $question->siswa[0]->pivot->jawaban }} <br>
                    <b>Nilai : </b> {{ $question->siswa[0]->pivot->nilai }} <br>
                </p>
                @else
                <a href="{{ route('question.show', $question->id) }}" class="btn btn-primary">Kerjakan</a>
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif
@stop