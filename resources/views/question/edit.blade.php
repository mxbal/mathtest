@extends('layouts.master', ['title' => 'Edit Soal'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">
                <form action="{{ route('question.update', $question->id) }}" method="post">
                    @method('PATCH')
                    @csrf

                    @include('question.form')
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')

@endpush