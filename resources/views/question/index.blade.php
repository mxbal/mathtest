@extends('layouts.master', ['title' => 'Data Soal'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">
                <a href="{{ route('question.create') }}" class="btn btn-primary mb-3">Tambah Soal</a>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Soal</th>
                                <th>Batas Atas</th>
                                <th>Batas Bawah</th>
                                <th>Jawaban</th>
                                <th>Bobot</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($questions as $question)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $question->nama }}</td>
                                <td>{{ $question->soal }}</td>
                                <td>{{ $question->batas_atas }}</td>
                                <td>{{ $question->batas_bawah }}</td>
                                <td>{{ $question->jawaban }}</td>
                                <td>{{ $question->bobot_nilai }}</td>
                                <td>
                                    <a href="{{ route('question.edit', $question->id) }}" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>

                                    <form action="{{ route('question.destroy', $question->id) }}" method="post" class="d-inline">
                                        @method('DELETE')
                                        @csrf

                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data Admin ?')"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script>
    $(document).ready(function() {
        $("#data-table").DataTable()
    })
</script>
@endpush