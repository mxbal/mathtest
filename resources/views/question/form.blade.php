<div class="form-group">
    <label for="nama">Nama Soal</label>
    <input type="text" name="nama" id="nama" class="form-control" value="{{ $question->nama ?? old('nama') }}">

    @error('nama')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="soal">Soal</label>
    <textarea name="soal" id="soal" rows="3" class="form-control">{{ $question->soal ?? old('soal') }}</textarea>

    @error('soal')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="jawaban">Jawaban</label>
    <input type="text" name="jawaban" id="jawaban" class="form-control" value="{{ $question->jawaban ?? old('jawaban') }}">

    @error('jawaban')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="batas_atas">Batas Atas</label>
    <input type="number" name="batas_atas" id="batas_atas" class="form-control" value="{{ $question->batas_atas ?? old('batas_atas') }}">

    @error('batas_atas')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="batas_bawah">Batas Bawah</label>
    <input type="number" name="batas_bawah" id="batas_bawah" class="form-control" value="{{ $question->batas_bawah ?? old('batas_bawah') }}">

    @error('batas_bawah')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="bobot_nilai">Bobot Nilai</label>
    <input type="number" name="bobot_nilai" id="bobot_nilai" class="form-control" value="{{ $question->bobot_nilai ?? old('bobot_nilai') }}">

    @error('bobot_nilai')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>