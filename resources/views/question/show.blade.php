@extends('layouts.master', ['title' => 'Detail Soal'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">
                <h5 class="mb-3">{{ $question->nama }}</h5>

                <p class="card-text">
                    {{ $question->soal }}
                </p>

                <form action="" method="post" id="form-question">
                    @csrf

                    <input type="hidden" name="siswa_id" value="{{ auth()->user()->siswa->id }}">
                    <input type="hidden" name="question_id" value="{{ $question->id }}">
                    <input type="hidden" name="nilai" id="nilai" value="0">

                    <div class="form-group">
                        <label for="deskripsi">Deskripsi Jawaban</label>
                        <textarea name="deskripsi" id="deskripsi" rows="3" class="form-control">{{ old('deskripsi') }}</textarea>

                        @error('deskripsi')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="jawaban">Jawaban</label>
                        <input type="text" name="jawaban" id="jawaban" class="form-control" value="{{ old('jawaban') }}">

                        @error('jawaban')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-form" disabled>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#form-question").on("keypress", function(event) {
        var keyPressed = event.keyCode || event.which;
        if (keyPressed === 13) {
            event.preventDefault();
            return false;
        }
    });

    $("#jawaban").on('keyup', function() {
        $(".btn-form").attr('disabled', 'disabled');

        let qid = "{{ $question->id }}";
        let jawaban = $(this).val();

        $.ajax({
            url: "/question/find/" + qid,
            type: 'GET',
            success: function(response) {
                let question = response.question;

                if (jawaban <= question.batas_atas && jawaban >= question.batas_bawah) {
                    $("#form-question").attr('action', '{{ route("question.answer") }}')
                    $(".btn-form").removeAttr(`disabled`);
                }

                if (jawaban == question.jawaban) {
                    $("#nilai").val(question.bobot_nilai)
                } else {
                    $("#nilai").val(0)
                }
            }
        })
    })
</script>
@endpush