@extends('layouts.master', ['title' => 'Data Siswa'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">
                <a href="{{ route('siswa.create') }}" class="btn btn-primary mb-3">Tambah Siswa</a>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nis</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($siswa as $sw)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $sw->nis }}</td>
                                <td>{{ $sw->nama }}</td>
                                <td>{{ $sw->kelas }}</td>
                                <td>
                                    <a href="{{ route('siswa.edit', $sw->id) }}" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>

                                    <form action="{{ route('siswa.destroy', $sw->id) }}" method="post" class="d-inline">
                                        @method('DELETE')
                                        @csrf

                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data Admin ?')"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script>
    $(document).ready(function() {
        $("#data-table").DataTable()
    })
</script>
@endpush