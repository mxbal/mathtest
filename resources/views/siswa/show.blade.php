@extends('layouts.master', ['title' => 'Detail Nilai Siswa'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Soal</th>
                                <th>Deskripsi</th>
                                <th>Jawaban</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($siswa->questions as $question)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $question->soal }}</td>
                                <td>{{ $question->pivot->deskripsi }}</td>
                                <td>{{ $question->pivot->jawaban }}</td>
                                <td>{{ $question->pivot->nilai }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script>
    $(document).ready(function() {
        $("#data-table").DataTable()
    })
</script>
@endpush