@extends('layouts.master', ['title' => 'Data Nilai Siswa'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nis</th>
                                <th>Nama</th>
                                <th>Nilai</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($siswa as $sw)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $sw->nis }}</td>
                                <td>{{ $sw->nama }}</td>
                                <td>{{ $sw->questions()->sum('nilai') }}</td>
                                <td>
                                    <a title="Detail" href="{{ route('siswa.show', $sw->id) }}" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></a>
                                    <a title="Reset" href="{{ route('siswa.reset', $sw->id) }}" class="btn btn-sm btn-success" onclick="return confirm('Reset nilai siswa ?')"><i class="fas fa-sync"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script>
    $(document).ready(function() {
        $("#data-table").DataTable()
    })
</script>
@endpush