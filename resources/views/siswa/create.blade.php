@extends('layouts.master', ['title' => 'Tambah Siswa'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">
                <form action="{{ route('siswa.store') }}" method="post">
                    @csrf

                    @include('siswa.form')
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')

@endpush