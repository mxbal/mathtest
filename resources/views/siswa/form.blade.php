<div class="form-group">
    <label for="nis">Nis</label>
    <input type="number" name="nis" id="nis" class="form-control" value="{{ $siswa->nis ?? old('nis') }}">

    @error('nis')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" name="nama" id="nama" class="form-control" value="{{ $siswa->nama ?? old('nama') }}">

    @error('nama')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="kelas">Kelas</label>
    <input type="text" name="kelas" id="kelas" class="form-control" value="{{ $siswa->kelas ?? old('kelas') }}">

    @error('kelas')
    <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>