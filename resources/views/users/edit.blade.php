@extends('layouts.master', ['title' => 'Edit Admin'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">
                <form action="{{ route('users.update', $user->id) }}" method="post">
                    @method('PATCH')
                    @csrf

                    @include('users.form')
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')

@endpush