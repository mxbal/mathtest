@extends('layouts.master', ['title' => 'Data Admin'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-body">
                <a href="{{ route('users.create') }}" class="btn btn-primary mb-3">Tambah Admin</a>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->name }}</td>
                                <td>
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-success"><i class="fas fa-edit"></i></a>

                                    <form action="{{ route('users.destroy', $user->id) }}" method="post" class="d-inline">
                                        @method('DELETE')
                                        @csrf

                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Hapus data Admin ?')"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script>
    $(document).ready(function() {
        $("#data-table").DataTable()
    })
</script>
@endpush