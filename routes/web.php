<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes([
    'register' => false
]);

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/profile', [DashboardController::class, 'profile'])->name('profile');
    Route::post('/profile/{user:id}/update', [DashboardController::class, 'update'])->name('profile.update');
    Route::resource('users', UserController::class)->middleware('isadmin');
    Route::get('/nilai', [SiswaController::class, 'nilai'])->name('nilai.index')->middleware('isadmin');
    Route::get('/siswa/reset/{siswa:id}', [SiswaController::class, 'reset'])->name('siswa.reset')->middleware('isadmin');
    Route::resource('siswa', SiswaController::class)->middleware('isadmin');
    Route::post('/question/answer', [QuestionController::class, 'answer'])->name('question.answer');
    Route::get('/question/find/{question:id}', [QuestionController::class, 'find'])->name('question.find');
    Route::resource('question', QuestionController::class);
});

Route::get('/install', function () {
    shell_exec('composer install');
    shell_exec('cp .env.example .env');
    Artisan::call('key:generate');
    Artisan::call('migrate:fresh --seed');
    Artisan::call('cache:clear');
});
